Quick start
-----------

 * "/usr/share/doc/enhanceio/README.txt.gz"
 * eio_cli(8)

Example:
    sudo eio_cli create -d /dev/disk/by-id/slow-rotational-hdd -s /dev/disk/by-id/fast-ssd-cache -c ENH_CACHE0 -p lru -m ro

In the above example we use safest and often fastest "read-only" mode
which maintainer recommends to try first. In "write-through" mode all
"slow" device' reads and writes will be written to cache which might
become a bottleneck on heavy IO. Remember that on cache miss maximum
read speed will be less or equal SSD write speed.


Caveats
-------

To avoid potential problems with "/dev/sd[a-z]" device assignments it is recommended
to use persistent devices i.e. "/dev/disk/by-id/", "/dev/disk/by-path/"
or "/dev/disk/by-uuid/".

blkid(8) utility may be useful to find UUID of an existing file system to cache.


Write-back mode: !!!--WARNING--!!!
----------------------------------

In order to avoid data loss maintainer recommends to avoid using
write-back mode at all.
If you ever consider using write-back mode make sure you read

    "/usr/share/doc/enhanceio/README.txt.gz"

particularly "Using a Write-back cache" paragraph as well as

    "/usr/share/doc/enhancio/Persistence.txt"

to learn about all the risks and implications.

Sample udev(7) template is available in "/usr/share/doc/enhanceio/examples".


Troubleshooting
---------------

`modprobe enhanceio`
> ERROR: could not insert 'enhanceio': Exec format error

Probably you need to reboot after installing updated kernel: DKMS built
modules using installed headers from newer kernel.

EnhanceIO requires Linux kernel 3.7+
